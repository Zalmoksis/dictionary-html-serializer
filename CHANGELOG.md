# Changelog

All notable changes to this project will be documented in this file
in the format of [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.12.0] — 2019-03-06
### Added
- Support for `SoundChange`

## [0.11.0] — 2019-12-20
### Added
- Support for labels (`Variety`, `Register`, `Domain`) and improved `HomographIndex`

## [0.10.0] — 2019-12-10
### Added
- Support for references: `Synonym`, `Antonym` and `Derivative`

## [0.9.3] — 2019-12-09
### Added
- Testing lowest dependencies
### Fixed
- Escaping HTML special characters

## [0.9.2] — 2019-12-03
### Fixed
- Removed redundant installation of PHP Mongo extension in Gitlab CI

## [0.9.1] — 2019-12-03
### Changed
- Less noise in Gitlab CI

## [0.9.0] — 2019-12-01
## Added
- Gitlab CI integration
## Changed
- PHP requirement upgrade to 7.4
- Strong typing of properties
- Several classes are now now final
- Improved configuration of PHP Unit

## [0.8.0] - 2019-02-24
### Added
- Support for etymology (`Etymon` and `Cognate`) and `Context`.

## [0.7.5] — 2019-02-06
### Changed
- Increased allowed version range of Dictionary Model

## [0.7.4] — 2019-01-21
### Added
- Collocations test

## [0.7.3] — 2019-01-21
### Added
- Licence

## [0.7.2] — 2019-01-21
### Added
- Collocation tests

## [0.7.1] — 2019-01-21
### Added
- This change log

## [0.7.0] — 2019-01-20
### Changed
- Internal structure now leverages node collections (Nodes) and values (Value) to speed up the development process.
