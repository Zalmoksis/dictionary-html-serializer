# HTML serializer of dictionary entries.

## Description

This is a tool allowing to generate a general purpose HTML from dictionary data as described by [zalmoksis/dictionary](https://gitlab.com/Zalmoksis/dictionary).

## Samples
 - [Entry](http://dict-html-demo.zalmoksis.pl/entry.php)
