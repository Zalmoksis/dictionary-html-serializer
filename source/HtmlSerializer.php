<?php

namespace Zalmoksis\Dictionary\Serializers\Html;

use Closure;
use Zalmoksis\Dictionary\Model\{Collocation, Entry, Sense};
use Zalmoksis\Dictionary\Serializers\Html\Subserializers\Subserializers;

use function ob_start, ob_get_clean;

class HtmlSerializer {
    protected Subserializers $subserializers;

    function __construct(Closure $linkGenerator) {
        $this->subserializers = new Subserializers($linkGenerator);
    }

    function serializeEntry(Entry $entry): string {
        ob_start();
        $this->subserializers->serialize($entry);
        return ob_get_clean();
    }

    function serializeSense(Sense $sense): string {
        ob_start();
        $this->subserializers->serialize($sense);
        return ob_get_clean();
    }

    function serializeCollocation(Collocation $collocation): string {
        ob_start();
        $this->subserializers->serialize($collocation);
        return ob_get_clean();
    }
}
