<?php

namespace Zalmoksis\Dictionary\Serializers\Html\Subserializers;

abstract class Subserializer {
    protected Subserializers $subserializers;

    function __construct(Subserializers $subserializers) {
        $this->subserializers = $subserializers;
    }

    protected function serializeChild(object $object = null): void {
        $this->subserializers->serialize($object);
    }

    protected function generateLink(string $headword): string {
        return $this->subserializers->getLinkGenerator()($headword);
    }

    protected function generateClass(string $nodeName): string {
        return str_replace(' ', '_', $nodeName);
    }
}
