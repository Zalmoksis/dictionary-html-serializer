<?php

namespace Zalmoksis\Dictionary\Serializers\Html\Subserializers;

use Zalmoksis\Dictionary\Model\Sense;

final class SenseSubserializer extends Subserializer {

    function serialize(Sense $sense): void {
        echo '<div class="' . $this->generateClass($sense::NODE_NAME) . '">' . "\n";

        // Usage
        if ($sense->getVarieties() || $sense->getRegisters() || $sense->getDomains() || $sense->getContext()) {
            echo '<div class="usage">' . "\n";

            $this->serializeChild($sense->getVarieties());
            $this->serializeChild($sense->getRegisters());
            $this->serializeChild($sense->getDomains());
            $this->serializeChild($sense->getContext());

            echo '</div>' . "\n";
        }

        // Definition
        $this->serializeChild($sense->getDefinition());
        $this->serializeChild($sense->getTranslations());
        $this->serializeChild($sense->getSynonyms());
        $this->serializeChild($sense->getAntonyms());

        // Subdivision
        $this->serializeChild($sense->getCollocations());
        $this->serializeChild($sense->getSenses());

        echo "</div>\n";
    }
}
