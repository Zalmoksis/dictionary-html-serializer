<?php

namespace Zalmoksis\Dictionary\Serializers\Html\Subserializers;

use Zalmoksis\Dictionary\Model\{Cognate, Etymon, Node};

class CrossLanguageReferenceSubserializer extends Subserializer {

    /**
     * @var Etymon | Cognate $crossLanguageReference
     */
    function serialize(Node $crossLanguageReference): void {
        echo '<div class="' . $this->generateClass($crossLanguageReference::NODE_NAME) . '">' . "\n";

        $this->serializeChild($crossLanguageReference->getLanguage());
        $this->serializeChild($crossLanguageReference->getLemma());
        $this->serializeChild($crossLanguageReference->getGloss());

        echo "</div>\n";
    }
}
