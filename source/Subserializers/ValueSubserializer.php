<?php

namespace Zalmoksis\Dictionary\Serializers\Html\Subserializers;

use Zalmoksis\Dictionary\Model\Value;

final class ValueSubserializer extends Subserializer {

    function serialize(Value $value): void {
        echo '<span class="' . $this->generateClass($value::NODE_NAME) . '">'
            . htmlspecialchars($value->getValue())
            . "</span>\n";
    }
}
