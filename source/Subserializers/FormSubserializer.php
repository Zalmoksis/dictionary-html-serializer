<?php

namespace Zalmoksis\Dictionary\Serializers\Html\Subserializers;

use LogicException;
use Zalmoksis\Dictionary\Model\{
    Form,
    FormGroup,
    FormLabel,
    FormNode,
    Headword,
};
use Zalmoksis\Dictionary\Model\Collections\{
    FormNodes,
    Headwords,
};

final class FormSubserializer extends Subserializer {

    function serialize(FormNodes $formNodes): void {
        echo '<div class="forms">' . "\n";

        $this->serializeFormNodes($formNodes);

        echo "</div>\n";
    }

    function serializeOne(FormNode $formNode): void {
        $this->serializeFormLabel($formNode->getFormLabel());

        switch (get_class($formNode)) {
            case FormGroup::class:
                /** @var FormGroup $formNode */
                $this->serializeFormNodes($formNode->getFormNodes());
                break;
            case Form::class:
                /** @var Form $formNode */
                $this->serializeHeadwords($formNode->getHeadwords());
                break;
            default:
                throw new LogicException('Unexpected FormNode: ' . get_class($formNode));
        }
    }

    protected function serializeFormNodes(FormNodes $formNodes): void {
        foreach ($formNodes as $formNode) {
            $this->serializeOne($formNode);
        }
    }

    protected function serializeHeadwords(Headwords $headwords): void {
        foreach ($headwords as $headword) {
            $this->serializeHeadword($headword);
        }
    }

    protected function serializeHeadword(Headword $headword): void {
        echo '<span class="form">' . htmlspecialchars($headword->getValue()) . "</span>\n";
    }

    protected function serializeFormLabel(FormLabel $formLabel): void {
        echo '<span class="form-label">' . htmlspecialchars($formLabel->getValue()) . "</span>\n";
    }
}
