<?php

namespace Zalmoksis\Dictionary\Serializers\Html\Subserializers;

use Zalmoksis\Dictionary\Model\Collocation;

final class CollocationSubserializer extends Subserializer {

    function serialize(Collocation $collocation): void {
        echo '<div class="' . $this->generateClass($collocation::NODE_NAME) . '">' . "\n";

        // Headwords
        $this->serializeChild($collocation->getHeadwords());
        $this->serializeChild($collocation->getPronunciations());

        // Usage
        if ($collocation->getVarieties() || $collocation->getRegisters() || $collocation->getDomains()) {
            echo '<div class="usage">' . "\n";

            $this->serializeChild($collocation->getVarieties());
            $this->serializeChild($collocation->getRegisters());
            $this->serializeChild($collocation->getDomains());

            echo '</div>' . "\n";
        }

        // Definition
        $this->serializeChild($collocation->getDefinition());
        $this->serializeChild($collocation->getTranslations());
        $this->serializeChild($collocation->getSenses());

        echo "</div>\n";
    }
}
