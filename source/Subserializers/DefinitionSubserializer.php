<?php

namespace Zalmoksis\Dictionary\Serializers\Html\Subserializers;

use Zalmoksis\Dictionary\Model\Definition;

final class DefinitionSubserializer extends Subserializer {

    function serialize(Definition $definition): void {
        echo '<div class="' . $this->generateClass($definition::NODE_NAME) . '">'
            . htmlspecialchars($definition->getValue())
            . "</div>\n";
    }
}
