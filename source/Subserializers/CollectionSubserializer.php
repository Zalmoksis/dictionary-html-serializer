<?php

namespace Zalmoksis\Dictionary\Serializers\Html\Subserializers;

use Zalmoksis\Dictionary\Model\Collections\Nodes;

final class CollectionSubserializer extends Subserializer {

    function serialize(Nodes $nodes): void {
        echo '<div class="' . $this->generateClass($nodes::NODE_COLLECTION_NAME) . '">' . "\n";

        foreach ($nodes as $node) {
            $this->serializeChild($node);
        }

        echo "</div>\n";
    }
}
