<?php

namespace Zalmoksis\Dictionary\Serializers\Html\Subserializers;

use Zalmoksis\Dictionary\Model\Reference;

class ReferenceSubserializer extends Subserializer {
    function serialize(Reference $reference): void {
        echo '<a class="' . $this->generateClass($reference::NODE_NAME) . '"'
            . ' href="' . $this->generateLink($reference->getHeadword()) . '">'
            . htmlspecialchars($reference->getHeadword())
            . '</a>'
            . "\n";
    }
}
