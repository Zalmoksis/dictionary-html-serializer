<?php

namespace Zalmoksis\Dictionary\Serializers\Html\Subserializers;

use Zalmoksis\Dictionary\Model\Entry;

final class EntrySubserializer extends Subserializer {

    function serialize(Entry $entry): void {
        echo '<div class="' . $this->generateClass($entry::NODE_NAME) . '">' . "\n";

        // Headword
        $this->serializeChild($entry->getHeadwords());
        $this->serializeChild($entry->getPronunciations());
        $this->serializeChild($entry->getCategories());
        $this->serializeChild($entry->getFormNodes());

        // Usage
        if ($entry->getVarieties() || $entry->getRegisters() || $entry->getDomains()) {
            echo '<div class="usage">' . "\n";

            $this->serializeChild($entry->getVarieties());
            $this->serializeChild($entry->getRegisters());
            $this->serializeChild($entry->getDomains());

            echo '</div>' . "\n";
        }

        // Definition
        $this->serializeChild($entry->getDefinition());
        $this->serializeChild($entry->getTranslations());
        $this->serializeChild($entry->getSynonyms());
        $this->serializeChild($entry->getAntonyms());

        // Subdivisions
        $this->serializeChild($entry->getCollocations());
        $this->serializeChild($entry->getSenses());

        // Etymology and word formation
        $this->serializeChild($entry->getDerivatives());
        $this->serializeChild($entry->getEtymons());
        $this->serializeChild($entry->getSoundChanges());
        $this->serializeChild($entry->getCognates());

        echo "</div>\n";
    }
}
