<?php

namespace Zalmoksis\Dictionary\Serializers\Html\Subserializers;

use Closure;
use LogicException;
use Zalmoksis\Dictionary\Model\{
    Antonym,
    Category,
    Cognate,
    Collocation,
    Context,
    Definition,
    Derivative,
    Domain,
    Entry,
    Etymon,
    Gloss,
    Headword,
    Language,
    Lemma,
    Pronunciation,
    Register,
    Sense,
    SoundChange,
    Synonym,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Cognates,
    Collocations,
    Derivatives,
    Domains,
    Etymons,
    FormNodes,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    SoundChanges,
    Synonyms,
    Translations,
    Varieties,
};

class Subserializers {
    protected array $serializersByClass = [
        Entry::class          => EntrySubserializer::class,

        Senses::class         => CollectionSubserializer::class,
        Collocations::class   => CollectionSubserializer::class,
        Headwords::class      => CollectionSubserializer::class,
        Pronunciations::class => CollectionSubserializer::class,
        Categories::class     => CollectionSubserializer::class,
        FormNodes::class      => FormSubserializer::class,
        Varieties::class      => CollectionSubserializer::class,
        Registers::class      => CollectionSubserializer::class,
        Domains::class        => CollectionSubserializer::class,
        Translations::class   => CollectionSubserializer::class,
        Synonyms::class       => CollectionSubserializer::class,
        Antonyms::class       => CollectionSubserializer::class,
        Derivatives::class    => CollectionSubserializer::class,
        Etymons::class        => CollectionSubserializer::class,
        SoundChanges::class   => CollectionSubserializer::class,
        Cognates::class       => CollectionSubserializer::class,

        Sense::class          => SenseSubserializer::class,
        Collocation::class    => CollocationSubserializer::class,
        Etymon::class         => CrossLanguageReferenceSubserializer::class,
        Cognate::class        => CrossLanguageReferenceSubserializer::class,

        Synonym::class        => ReferenceSubserializer::class,
        Antonym::class        => ReferenceSubserializer::class,
        Derivative::class     => ReferenceSubserializer::class,

        Headword::class       => ValueSubserializer::class,
        Pronunciation::class  => ValueSubserializer::class,
        Category::class       => ValueSubserializer::class,
        Variety::class        => ValueSubserializer::class,
        Register::class       => ValueSubserializer::class,
        Domain::class         => ValueSubserializer::class,
        Context::class        => ValueSubserializer::class,
        Definition::class     => DefinitionSubserializer::class,
        Translation::class    => ValueSubserializer::class,
        Language::class       => ValueSubserializer::class,
        Lemma::class          => ValueSubserializer::class,
        Gloss::class          => ValueSubserializer::class,
        SoundChange::class    => ValueSubserializer::class,
    ];

    private array $subserializers = [];
    private Closure $linkGenerator;

    function __construct(Closure $linkGenerator) {
        $this->linkGenerator = $linkGenerator;
    }

    function serialize(object $object = null): void {
        if (!$object) {
            return;
        }

        $this->getSubserializerFor($object)->serialize($object);
    }

    function getLinkGenerator(): Closure {
        return $this->linkGenerator;
    }

    private function getSubserializerFor(object $object) {
        if (!isset($this->serializersByClass[get_class($object)])) {
            throw new LogicException('No handler found for: ' . get_class($object) . '.');
        }

        $subserializerClass = $this->serializersByClass[get_class($object)];

        if (!isset($this->subserializers[$subserializerClass])) {
            $this->subserializers[$subserializerClass] = new $subserializerClass($this);
        }

        return $this->subserializers[$subserializerClass];
    }
}
