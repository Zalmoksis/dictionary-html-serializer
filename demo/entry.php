<?php

use Zalmoksis\Dictionary\Serializers\Html\HtmlSerializer;

require_once __DIR__ . '/../vendor/autoload.php';

?>
<!DOCTYPE html>
<html>
<head>
<title>Zalmoksis\Dictionary: HTML Serializer Demo</title>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.headword { font-weight: bold }
.definition { font-style: italic }
.form-label { font-style: italic }
.form { font-weight: bold }
.senses { margin-left: 1em }
</style>
</head>
<body>
<h1>HTML Serializer Demo</h1>
<?= (new HtmlSerializer(fn (string $headword) => "/"))->serializeEntry(require __DIR__ . '/../tests/samples/model/entry.php') ?>
</body>
</html>
