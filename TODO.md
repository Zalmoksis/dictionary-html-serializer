# To do

## Bugfix
- collocation tests
- licence
- tests for non-ASCII characters
- tests for etymologies

## Minor
- homograph index support
- subserializer mapping in HtmlSerializer (facade)
- renaming to `HtmlRenderer`