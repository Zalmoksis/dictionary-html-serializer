<?php

/**
 * @noinspection PhpDocSignatureInspection
 * @noinspection PhpUnhandledExceptionInspection
 */

namespace Zalmoksis\Dictionary\Serializers\Html\Tests\Unit\Subserializers;

use PHPUnit\Framework\{MockObject\MockObject, TestCase};
use Zalmoksis\Dictionary\Serializers\Html\Subserializers\{ReferenceSubserializer, Subserializers};
use Zalmoksis\Dictionary\Model\Reference;

final class ReferenceSubserializerTest extends TestCase {
    protected ReferenceSubserializer $referenceSubserializer;

    /** @var Subserializers | MockObject */
    protected $subserializers;

    public function setUp(): void {
        $this->subserializers = $this->createMock(Subserializers::class);
        $this->subserializers->method('getLinkGenerator')->willReturn(
            fn ($headword): string => 'https://dictionary/search/' . rawurlencode($headword)
        );

        $this->referenceSubserializer = new ReferenceSubserializer($this->subserializers);
    }

    public function testSerializingHtmlSpecialCharacters(): void {
        ob_start();
        $this->referenceSubserializer->serialize(new class('<script language="JavaScript">') extends Reference {});
        $this->assertEquals(<<<HTML
            <a class="reference" href="https://dictionary/search/%3Cscript%20language%3D%22JavaScript%22%3E">&lt;script language=&quot;JavaScript&quot;&gt;</a>

            HTML,
            ob_get_clean()
        );
    }

    public function testSerializingNonAsciiCharacters(): void {
        ob_start();
        $this->referenceSubserializer->serialize(new class('ˈrɛfrəns') extends Reference {});
        $this->assertEquals(<<<HTML
            <a class="reference" href="https://dictionary/search/%CB%88r%C9%9Bfr%C9%99ns">ˈrɛfrəns</a>

            HTML,
            ob_get_clean()
        );
    }
}
