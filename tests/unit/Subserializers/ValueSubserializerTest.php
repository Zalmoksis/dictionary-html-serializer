<?php

/**
 * @noinspection PhpDocSignatureInspection
 * @noinspection PhpUnhandledExceptionInspection
 */

namespace Zalmoksis\Dictionary\Serializers\Html\Tests\Unit\Subserializers;

use PHPUnit\Framework\{MockObject\MockObject, TestCase};
use Zalmoksis\Dictionary\Serializers\Html\Subserializers\{Subserializers, ValueSubserializer};
use Zalmoksis\Dictionary\Model\Value;

final class ValueSubserializerTest extends TestCase {
    protected ValueSubserializer $valueSubserializer;

    /** @var Subserializers | MockObject */
    protected $subserializers;

    public function setUp(): void {
        $this->subserializers = $this->createMock(Subserializers::class);

        $this->valueSubserializer = new ValueSubserializer($this->subserializers);
    }

    public function testSerializingHtmlSpecialCharacters(): void {
        ob_start();
        $this->valueSubserializer->serialize(new class('<script language="JavaScript">') extends Value {});
        $this->assertEquals(<<<HTML
            <span class="value">&lt;script language=&quot;JavaScript&quot;&gt;</span>

            HTML,
            ob_get_clean()
        );
    }

    public function testSerializingNonAsciiCharacters(): void {
        ob_start();
        $this->valueSubserializer->serialize(new class('ˈvæljuː') extends Value {});
        $this->assertEquals(<<<HTML
            <span class="value">ˈvæljuː</span>

            HTML,
            ob_get_clean()
        );
    }
}
