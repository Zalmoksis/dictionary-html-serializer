<?php

use Zalmoksis\Dictionary\Model\{
    Antonym,
    Collocation,
    Context,
    Definition,
    Domain,
    Headword,
    Pronunciation,
    Register,
    Sense,
    Synonym,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Collocations,
    Domains,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    Synonyms,
    Translations,
    Varieties,
};

return new Senses(
    (new Sense())
        ->setVarieties(new Varieties(
            new Variety('variety 1.1'),
            new Variety('variety 1.2'),
        ))
        ->setRegisters(new Registers(
            new Register('register 1.1'),
            new Register('register 1.2'),
        ))
        ->setDomains(new Domains(
            new Domain('domain 1.1'),
            new Domain('domain 1.2'),
        ))
        ->setContext(new Context('context 1'))
        ->setDefinition(new Definition('definition 1'))
        ->setTranslations(new Translations(
            new Translation('translation 1.1'),
            new Translation('translation 1.2'),
        ))
        ->setSynonyms(new Synonyms(
            new Synonym('synonym 1.1'),
            new Synonym('synonym 1.2'),
        ))
        ->setAntonyms(new Antonyms(
            new Antonym('antonym 1.1'),
            new Antonym('antonym 1.2'),
        ))
        ->setCollocations(new Collocations(
            (new Collocation())
                ->setHeadwords(new Headwords(
                    new Headword('headword 1.a.1'),
                    new Headword('headword 1.a.2'),
                ))
                ->setPronunciations(new Pronunciations(
                    new Pronunciation('pronunciation 1.a.1'),
                    new Pronunciation('pronunciation 1.a.2'),
                ))
                ->setDefinition(new Definition('definition 1.a'))
                ->setTranslations(new Translations(
                    new Translation('translation 1.a.1'),
                    new Translation('translation 1.a.2'),
                ))
                ->setSenses(new Senses(
                    (new Sense())
                        ->setTranslations(new Translations(
                            new Translation('translation 1.a.1.1'),
                        )),
                    (new Sense())
                        ->setTranslations(new Translations(
                            new Translation('translation 1.a.2.1'),
                        )),
                )),
            (new Collocation())
                ->setHeadwords(new Headwords(new Headword('headword b.1')))
                ->setTranslations(new Translations(
                    new Translation('translation b.1'),
                ))
        ))
        ->setSenses(new Senses(
            (new Sense())
                ->setDefinition(new Definition('definition 1.1'))
                ->setTranslations(new Translations(
                    new Translation('translation 1.1.1'),
                )),
            (new Sense())
                ->setDefinition(new Definition('definition 1.2'))
                ->setTranslations(new Translations(
                    new Translation('translation 1.2.1'),
                )),
        )),
    (new Sense())
        ->setDefinition(new Definition('definition 2'))
        ->setTranslations(new Translations(
            new Translation('translation 2.1'),
            new Translation('translation 2.2'),
        )),
);
