<?php

use Zalmoksis\Dictionary\Model\{Etymon, Gloss, Language, Lemma};

return (new Etymon())
    ->setLanguage(new Language('language 1'))
    ->setLemma(new Lemma('lemma 1'))
    ->setGloss(new Gloss('gloss 1'))
;
