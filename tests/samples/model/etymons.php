<?php

use Zalmoksis\Dictionary\Model\{Collections\Etymons, Etymon, Gloss, Language, Lemma};

return new Etymons(
    (new Etymon())
        ->setLanguage(new Language('language 1'))
        ->setLemma(new Lemma('lemma 1'))
        ->setGloss(new Gloss('gloss 1')),
    (new Etymon())
        ->setLanguage(new Language('language 2'))
        ->setLemma(new Lemma('lemma 2'))
        ->setGloss(new Gloss('gloss 2')),
    )
;
