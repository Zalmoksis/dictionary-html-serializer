<?php

use Zalmoksis\Dictionary\Model\{
    Collocation,
    Definition,
    Domain,
    Headword,
    Pronunciation,
    Register,
    Sense,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Domains,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    Translations,
    Varieties,
};

return (new Collocation())
    ->setHeadwords(new Headwords(
        new Headword('headword 1'),
        new Headword('headword 2')
    ))
    ->setPronunciations(new Pronunciations(
        new Pronunciation('pronunciation 1'),
        new Pronunciation('pronunciation 2'),
    ))
    ->setVarieties(new Varieties(
        new Variety('variety 1'),
        new Variety('variety 2'),
    ))
    ->setRegisters(new Registers(
        new Register('register 1'),
        new Register('register 2'),
    ))
    ->setDomains(new Domains(
        new Domain('domain 1'),
        new Domain('domain 2'),
    ))
    ->setDefinition(new Definition('definition'))
    ->setTranslations(new Translations(
        new Translation('translation 1'),
        new Translation('translation 2'),
    ))
    ->setSenses(new Senses(
        (new Sense())
            ->setTranslations(new Translations(
                new Translation('translation 1.1'),
                new Translation('translation 1.2'),
            )),
        (new Sense())
            ->setTranslations(new Translations(
                new Translation('translation 2.1'),
                new Translation('translation 2.2'),
            )),
    ))
;
