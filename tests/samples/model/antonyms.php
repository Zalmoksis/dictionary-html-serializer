<?php

use Zalmoksis\Dictionary\Model\{Collections\Antonyms, Antonym};

return new Antonyms(
    new Antonym('antonym 1'),
    new Antonym('antonym 2'),
);
