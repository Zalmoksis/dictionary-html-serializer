<?php

use Zalmoksis\Dictionary\Model\Collections\Categories;
use Zalmoksis\Dictionary\Model\Category;

return (new Categories(
    new Category('category 1'),
    new Category('category 2'),
));
