<?php

use Zalmoksis\Dictionary\Model\{Collections\Translations, Translation};

return new Translations(
    new Translation('translation 1'),
    new Translation('translation 2'),
);
