<?php

use Zalmoksis\Dictionary\Model\{Collections\Pronunciations, Pronunciation};

return (new Pronunciations(
    new Pronunciation('pronunciation 1'),
    new Pronunciation('pronunciation 2'),
));
