<?php

use Zalmoksis\Dictionary\Model\{Cognate, Gloss, Language, Lemma};

return (new Cognate())
    ->setLanguage(new Language('language 1'))
    ->setLemma(new Lemma('lemma 1'))
    ->setGloss(new Gloss('gloss 1'))
;
