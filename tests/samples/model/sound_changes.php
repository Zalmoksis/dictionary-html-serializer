<?php

use Zalmoksis\Dictionary\Model\{Collections\SoundChanges, SoundChange};

return new SoundChanges(
    new SoundChange('sound change 1'),
    new SoundChange('sound change 2'),
);
