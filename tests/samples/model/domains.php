<?php

use Zalmoksis\Dictionary\Model\{Collections\Domains, Domain};

return new Domains(
    new Domain('domain 1'),
    new Domain('domain 2'),
);
