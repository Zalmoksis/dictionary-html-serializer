<?php

use Zalmoksis\Dictionary\Model\{Collections\Cognates, Cognate, Gloss, Language, Lemma};

return new Cognates(
    (new Cognate())
        ->setLanguage(new Language('language 1'))
        ->setLemma(new Lemma('lemma 1'))
        ->setGloss(new Gloss('gloss 1')),
    (new Cognate())
        ->setLanguage(new Language('language 2'))
        ->setLemma(new Lemma('lemma 2'))
        ->setGloss(new Gloss('gloss 2')),
    )
;
