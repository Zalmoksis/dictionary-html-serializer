<?php

use Zalmoksis\Dictionary\Model\{
    Collocation,
    Definition,
    Domain,
    Headword,
    Pronunciation,
    Register,
    Sense,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Collocations,
    Domains,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    Translations,
    Varieties,
};

return new Collocations(
    (new Collocation())
        ->setHeadwords(new Headwords(
            new Headword('headword a.1'),
            new Headword('headword a.2'),
        ))
        ->setPronunciations(new Pronunciations(
            new Pronunciation('pronunciation a.1'),
            new Pronunciation('pronunciation a.2'),
        ))
        ->setVarieties(new Varieties(
            new Variety('variety 1'),
            new Variety('variety 2'),
        ))
        ->setRegisters(new Registers(
            new Register('register 1'),
            new Register('register 2'),
        ))
        ->setDomains(new Domains(
            new Domain('domain 1'),
            new Domain('domain 2'),
        ))
        ->setDefinition(new Definition('definition a'))
        ->setTranslations(new Translations(
            new Translation('translation a.1'),
            new Translation('translation a.2'),
        ))
        ->setSenses(new Senses(
            (new Sense())
                ->setTranslations(new Translations(
                    new Translation('translation a.1.1'),
                    new Translation('translation a.1.2'),
                )),
            (new Sense())
                ->setTranslations(new Translations(
                    new Translation('translation a.2.1'),
                    new Translation('translation a.2.2'),
                )),
        )),
    (new Collocation())
        ->setHeadwords(new Headwords(
            new Headword('headword b.1'),
        ))
        ->setTranslations(new Translations(
            new Translation('translation b.1'),
        )),
);
