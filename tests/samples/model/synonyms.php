<?php

use Zalmoksis\Dictionary\Model\{Collections\Synonyms, Synonym};

return new Synonyms(
    new Synonym('synonym 1'),
    new Synonym('synonym 2'),
);
