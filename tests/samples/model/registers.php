<?php

use Zalmoksis\Dictionary\Model\{Collections\Registers, Register};

return new Registers(
    new Register('register 1'),
    new Register('register 2'),
);
