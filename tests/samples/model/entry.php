<?php

use Zalmoksis\Dictionary\Model\{
    Antonym,
    Category,
    Cognate,
    Collocation,
    Definition,
    Derivative,
    Domain,
    Entry,
    Etymon,
    Form,
    FormGroup,
    FormLabel,
    Gloss,
    Language,
    Lemma,
    Headword,
    Pronunciation,
    Register,
    Sense,
    SoundChange,
    Synonym,
    Translation,
    Variety,
};
use Zalmoksis\Dictionary\Model\Collections\{
    Antonyms,
    Categories,
    Collocations,
    Cognates,
    Derivatives,
    Domains,
    Etymons,
    FormNodes,
    Headwords,
    Pronunciations,
    Registers,
    Senses,
    SoundChanges,
    Synonyms,
    Translations,
    Varieties,
};

return (new Entry())
    ->setHeadwords(new Headwords(
        new Headword('headword 1'),
        new Headword('headword 2'),
    ))
    ->setPronunciations(new Pronunciations(
        new Pronunciation('pronunciation 1'),
        new Pronunciation('pronunciation 2'),
    ))
    ->setCategories(new Categories(
        new Category('category 1'),
        new Category('category 2'),
    ))
    ->setFormNodes(new FormNodes(
        (new FormGroup(new FormLabel('form label 1')))
            ->setFormNodes(new FormNodes(
                (new Form(new FormLabel('form label 1.1')))
                    ->setHeadwords(new Headwords(
                        new Headword('form 1.1'),
                    )),
                (new Form(new FormLabel('form label 1.2')))
                    ->setHeadwords(new Headwords(
                        new Headword('form 1.2'),
                    )),
            )),
        (new Form(new FormLabel('form label 2')))
            ->setHeadwords(new Headwords(new Headword('form 2')))
    ))
    ->setVarieties(new Varieties(
        new Variety('variety 1'),
        new Variety('variety 2'),
    ))
    ->setRegisters(new Registers(
        new Register('register 1'),
        new Register('register 2'),
    ))
    ->setDomains(new Domains(
        new Domain('domain 1'),
        new Domain('domain 2'),
    ))
    ->setDefinition(new Definition('definition'))
    ->setTranslations(new Translations(
        new Translation('translation 1'),
        new Translation('translation 2'),
    ))
    ->setSynonyms(new Synonyms(
        new Synonym('synonym 1'),
        new Synonym('synonym 2'),
    ))
    ->setAntonyms(new Antonyms(
        new Antonym('antonym 1'),
        new Antonym('antonym 2'),
    ))
    ->setCollocations(new Collocations(
        (new Collocation())
            ->setHeadwords(new Headwords(
                new Headword('headword a.1'),
                new Headword('headword a.2'),
            ))
            ->setPronunciations(new Pronunciations(
                new Pronunciation('pronunciation a.1'),
                new Pronunciation('pronunciation a.2'),
            ))
            ->setDefinition(new Definition('definition a'))
            ->setTranslations(new Translations(
                new Translation('translation a.1'),
                new Translation('translation a.2'),
            ))
            ->setSenses(new Senses(
                (new Sense())
                    ->setTranslations(new Translations(
                        new Translation('translation a.1.1'),
                    )),
                (new Sense())
                    ->setTranslations(new Translations(
                        new Translation('translation a.2.1'),
                    )),
            )),
        (new Collocation())
            ->setHeadwords(new Headwords(
                new Headword('headword b.1'),
            ))
            ->setTranslations(new Translations(
                new Translation('translation b.1'),
            )),
    ))
    ->setSenses(new Senses(
        (new Sense())
            ->setDefinition(new Definition('definition 1'))
            ->setTranslations(new Translations(
                new Translation('translation 1.1'),
                new Translation('translation 1.2'),
            ))
            ->setSenses(new Senses(
                (new Sense())
                    ->setDefinition(new Definition('definition 1.1'))
                    ->setTranslations(new Translations(
                        new Translation('translation 1.1.1'),
                    )),
                (new Sense())
                    ->setDefinition(new Definition('definition 1.2'))
                    ->setTranslations(new Translations(
                        new Translation('translation 1.2.1'),
                    )),
            )),
        (new Sense())
            ->setDefinition(new Definition('definition 2'))
            ->setTranslations(new Translations(
                new Translation('translation 2.1'),
                new Translation('translation 2.2'),
            )),
    ))
    ->setDerivatives(new Derivatives(
        new Derivative('derivative 1'),
        new Derivative('derivative 2'),
    ))
    ->setEtymons(new Etymons(
        (new Etymon())
            ->setLanguage(new Language('language 1'))
            ->setLemma(new Lemma('lemma 1'))
            ->setGloss(new Gloss('gloss 1')),
        (new Etymon())
            ->setLemma(new Lemma('lemma 2')),
    ))
    ->setSoundChanges(new SoundChanges(
        new SoundChange('sound change 1'),
        new SoundChange('sound change 2'),
    ))
    ->setCognates(new Cognates(
        (new Cognate())
            ->setLanguage(new Language('language 1'))
            ->setLemma(new Lemma('lemma 1'))
            ->setGloss(new Gloss('gloss 1')),
        (new Cognate())
            ->setLanguage(new Language('language 2'))
            ->setLemma(new Lemma('lemma 2'))
    ))
;
