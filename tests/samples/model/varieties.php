<?php

use Zalmoksis\Dictionary\Model\{Collections\Varieties, Variety};

return new Varieties(
    new Variety('variety 1'),
    new Variety('variety 2'),
);
