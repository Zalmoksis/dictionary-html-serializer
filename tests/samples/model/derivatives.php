<?php

use Zalmoksis\Dictionary\Model\{Collections\Derivatives, Derivative};

return new Derivatives(
    new Derivative('derivative 1'),
    new Derivative('derivative 2'),
);
