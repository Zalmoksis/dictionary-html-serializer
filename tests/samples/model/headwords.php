<?php

use Zalmoksis\Dictionary\Model\Collections\Headwords;
use Zalmoksis\Dictionary\Model\Headword;

return (new Headwords(
    new Headword('headword 1'),
    new Headword('headword 2'),
));
