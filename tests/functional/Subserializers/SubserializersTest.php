<?php

/**
 * @noinspection PhpDocSignatureInspection
 */

namespace Zalmoksis\Dictionary\Serializers\Html\Tests\Functional\Subserializers;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Serializers\Html\Subserializers\{
    Subserializers,
};

final class SubserializersTest extends TestCase {
    protected Subserializers $subserializers;

    public function setUp(): void {
        $this->subserializers = new Subserializers(
            fn (string $headword): string => 'https://dictionary/search/' . rawurlencode($headword)
        );
    }

    public function provideDataForSerialization(): array {
        return [
            'entry' => [
                'modelPath' => __DIR__ . '/../../samples/model/entry.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/entry.html',
            ],
            'senses' => [
                'modelPath' => __DIR__ . '/../../samples/model/senses.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/senses.html',
            ],
            'sense' => [
                'modelPath' => __DIR__ . '/../../samples/model/sense.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/sense.html',
            ],
            'collocations' => [
                'modelPath' => __DIR__ . '/../../samples/model/collocations.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/collocations.html',
            ],
            'collocation' => [
                'modelPath' => __DIR__ . '/../../samples/model/collocation.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/collocation.html',
            ],
            'headwords' => [
                'modelPath' => __DIR__ . '/../../samples/model/headwords.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/headwords.html',
            ],
            'headword' => [
                'modelPath' => __DIR__ . '/../../samples/model/headword.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/headword.html',
            ],
            'pronunciations' => [
                'modelPath' => __DIR__ . '/../../samples/model/pronunciations.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/pronunciations.html',
            ],
            'pronunciation' => [
                'modelPath' => __DIR__ . '/../../samples/model/pronunciation.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/pronunciation.html',
            ],
            'categories' => [
                'modelPath' => __DIR__ . '/../../samples/model/categories.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/categories.html',
            ],
            'category' => [
                'modelPath' => __DIR__ . '/../../samples/model/category.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/category.html',
            ],
            'varieties' => [
                'modelPath' => __DIR__ . '/../../samples/model/varieties.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/varieties.html',
            ],
            'variety' => [
                'modelPath' => __DIR__ . '/../../samples/model/variety.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/variety.html',
            ],
            'registers' => [
                'modelPath' => __DIR__ . '/../../samples/model/registers.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/registers.html',
            ],
            'register' => [
                'modelPath' => __DIR__ . '/../../samples/model/translation.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/translation.html',
            ],
            'domain' => [
                'modelPath' => __DIR__ . '/../../samples/model/translations.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/translations.html',
            ],
            'domains' => [
                'modelPath' => __DIR__ . '/../../samples/model/translation.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/translation.html',
            ],
            'definition' => [
                'modelPath' => __DIR__ . '/../../samples/model/definition.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/definition.html',
            ],
            'translations' => [
                'modelPath' => __DIR__ . '/../../samples/model/translations.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/translations.html',
            ],
            'translation' => [
                'modelPath' => __DIR__ . '/../../samples/model/translation.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/translation.html',
            ],
            'synonyms' => [
                'modelPath' => __DIR__ . '/../../samples/model/synonyms.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/synonyms.html',
            ],
            'synonym' => [
                'modelPath' => __DIR__ . '/../../samples/model/synonym.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/synonym.html',
            ],
            'antonyms' => [
                'modelPath' => __DIR__ . '/../../samples/model/antonyms.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/antonyms.html',
            ],
            'antonym' => [
                'modelPath' => __DIR__ . '/../../samples/model/antonym.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/antonym.html',
            ],
            'derivatives' => [
                'modelPath' => __DIR__ . '/../../samples/model/derivatives.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/derivatives.html',
            ],
            'derivative' => [
                'modelPath' => __DIR__ . '/../../samples/model/derivative.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/derivative.html',
            ],
            'etymons' => [
                'modelPath' => __DIR__ . '/../../samples/model/etymons.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/etymons.html',
            ],
            'etymon' => [
                'modelPath' => __DIR__ . '/../../samples/model/etymon.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/etymon.html',
            ],
            'sound changes' => [
                'modelPath' => __DIR__ . '/../../samples/model/sound_changes.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/sound_changes.html',
            ],
            'sound change' => [
                'modelPath' => __DIR__ . '/../../samples/model/sound_change.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/sound_change.html',
            ],
            'cognates' => [
                'modelPath' => __DIR__ . '/../../samples/model/cognates.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/cognates.html',
            ],
            'cognate' => [
                'modelPath' => __DIR__ . '/../../samples/model/cognate.php',
                'htmlPath'  => __DIR__ . '/../../samples/html/cognate.html',
            ],
        ];
    }

    /**
     * @dataProvider provideDataForSerialization
     */
    public function testSerialization(string $modelPath, string $htmlPath): void {
        ob_start();
        $this->subserializers->serialize(
            require $modelPath
        );

        $this->assertStringEqualsFile(
            $htmlPath,
            ob_get_clean()
        );
    }
}
