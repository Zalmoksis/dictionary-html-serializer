<?php

namespace Zalmoksis\Dictionary\Serializers\Html\Tests\Functional;

use PHPUnit\Framework\TestCase;
use Zalmoksis\Dictionary\Serializers\Html\HtmlSerializer;

final class HtmlSerializerTest extends TestCase {
    protected HtmlSerializer $serializer;

    function setUp(): void {
        $this->serializer = new HtmlSerializer(
            fn (string $headword): string => 'https://dictionary/search/' . rawurlencode($headword)
        );
    }

    function testEntrySerialization(): void {
        $this->assertStringEqualsFile(
            __DIR__ . '/../samples/html/entry.html',
            $this->serializer->serializeEntry(require __DIR__ . '/../samples/model/entry.php')
        );
    }

    function testSenseSerialization(): void {
        $this->assertStringEqualsFile(
            __DIR__ . '/../samples/html/sense.html',
            $this->serializer->serializeSense(require __DIR__ . '/../samples/model/sense.php')
        );
    }

    function testCollocationSerialization(): void {
        $this->assertStringEqualsFile(
            __DIR__ . '/../samples/html/collocation.html',
            $this->serializer->serializeCollocation(require __DIR__ . '/../samples/model/collocation.php')
        );
    }
}
